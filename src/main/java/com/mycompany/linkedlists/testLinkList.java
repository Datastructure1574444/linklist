/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.linkedlists;

/**
 *
 * @author HP
 */
public class testLinkList {
    public static void main(String[] args) {
        LinkedLists theList = new LinkedLists();
        theList.insertFirst(22, 2.99);
        theList.insertFirst(44, 4.99);
        theList.insertFirst(66, 6.99);
        theList.insertFirst(88, 8.99);
        theList.displayFirst();
        Link f = theList.find(44);
        if(f != null){
            System.out.println("Found Link with key " + f.iData);
        }else{
            System.out.println("Can't find Link");
        }
        Link d = theList.delete(66);
        if(d != null) {
            System.out.println("Deleted Link with key " + d.iData);
        }else{
            System.out.println("Can't delete link");
        }
        theList.displayFirst();
    }
}

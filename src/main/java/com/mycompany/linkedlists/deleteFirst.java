/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.linkedlists;

import java.util.LinkedList;

/**
 *
 * @author HP
 */
public class deleteFirst {
    public static void main(String[] args) {
        LinkedLists theList = new LinkedLists();
        theList.insertFirst(22, 2.99);
        theList.insertFirst(44, 4.99);
        theList.insertFirst(66, 6.99);
        theList.insertFirst(88, 8.99);
        theList.displayFirst();
        while(!theList.isEmpty()){
            Link aLink = theList.deleteFirst();
            System.out.println("Deleted ");
            aLink.displayLink();
            System.out.println("");
        }
        theList.displayFirst();
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.linkedlists;

/**
 *
 * @author HP
 */
public class Link {
    public int iData;
    public double dData;
    public Link next;
    
    public Link(int id, double dd){
        iData = id;
        dData = dd;
    }
    
    public void displayLink(){
        System.out.println("{" + iData + ", " + dData + "} ");
    }
}
